import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";

import NumberList from "./components/NumberList";
import NumberCreator from "./components/NumberCreator";

class App extends Component {
  state = {
    numberList: [],
    number: 10,
    count: 10,
  };

  addNumber = (number) => {
    const { count } = this.state;

    !!+number !== false
      ? this.setState({ count: +number })
      : this.setState({ count: 10 });
    const { numberList } = this.state;
    this.setState({
      numberList:
        numberList.length === 0
          ? [count]
          : [
              ...this.state.numberList,
              this.state.numberList[this.state.numberList.length - 1] + count,
            ],
    });
    console.log(this.state.count);
  };
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <button onClick={this.addNumber}>Mostre Div!</button>
          <NumberCreator
            estado={this.state.number}
            onClick={this.addNumber}
          ></NumberCreator>

          <NumberList list={this.state.numberList}></NumberList>
        </header>
      </div>
    );
  }
}

export default App;
