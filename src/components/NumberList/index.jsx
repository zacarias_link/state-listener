import { Component } from "react";
import Number from "../Number";
class NumberList extends Component {
  render() {
    return (
      <>
        {this.props.list.map((number, index, array) => (
          <Number number={number}></Number>
        ))}
      </>
    );
  }
}

export default NumberList;
