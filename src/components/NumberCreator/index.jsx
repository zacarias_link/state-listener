import { Component } from "react";

class NumberCreator extends Component {
  state = {
    number: 10,
  };
  //   inClick = () => {
  //     this.props.onClick;
  //   };
  //   addNumber = () => {
  //     const { number } = this.state;
  //     const { numberList } = this.state;
  //     this.setState({
  //       numberList:
  //         numberList.length === 0
  //           ? [number]
  //           : [
  //               ...this.state.numberList,
  //               this.state.numberList[this.state.numberList.length - 1] + number,
  //             ],
  //     });
  //   };
  mudarNumero = (e) => {
    const { number } = this.state;
    this.setState({ number: e.target.value });
  };
  render() {
    return (
      <>
        <input
          value={this.state.number}
          type="number"
          onChange={this.mudarNumero}
        />
        <button onClick={() => this.props.onClick(this.state.number)}>
          mostre novo
        </button>
      </>
    );
  }
}

export default NumberCreator;
